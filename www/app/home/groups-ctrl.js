(function () {
    'use strict';

    angular.module('eliteApp').controller('GroupsCtrl', ['$state', 'eliteApi', GroupsCtrl]);
    function GroupsCtrl($state, eliteApi) {
        var vm = this;

        eliteApi.getGroups().then(function(data){
            vm.leagues = data;
        });

        vm.selectGroup = function(id){
            eliteApi.setGroupId(id);
            $state.go("app.teams");
        }

    };
})();