angular.module("eliteApp", ["ionic", "angular-data.DSCacheFactory"])
// .run(function($cordovaStatusbar) {
//   $cordovaStatusbar.overlaysWebView(true)
 
//   $cordovaStatusBar.style(2) //Black, transulcent 
// })
.run(function($ionicPlatform, DSCacheFactory) {
  $ionicPlatform.ready(function() {
// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
// for form inputs)
  if (window.cordova && window.cordova.plugins.Keyboard) {
    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
  }
  if (window.StatusBar) {
// org.apache.cordova.statusbar required
StatusBar.styleDefault();
}

DSCacheFactory("leagueDataCache", {
  storageMode: "localStorage",
  maxAge: 1,
  deleteOnExpire: "aggressive"
});
DSCacheFactory("leaguesCache", {
  storageMode: "localStorage",
  maxAge: 1,
  deleteOnExpire: "aggressive"
});
DSCacheFactory("myTeamsCache", {
  storageMode: "localStorage"
});
DSCacheFactory("staticCache", {
  storageMode: "localStorage"
});
});
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

 
  
  .state('group-detail', {
    url: "/group/:id",
    views: {
      'mainContent': {
        templateUrl: "app/groups/group-detail.html"
      }
    }
  })
  .state('app', {
    abstract: true,
    url: "/app",
    templateUrl: "app/layout/menu-layout.html"
  })
 
 
  .state('app.game', {
    url: "/game/:id",
    views: {
      'mainContent': {
        templateUrl: "app/game/game.html"
      }
    }
  })
 
  .state('app.rules', {
    url: "/rules",
    views: {
      'mainContent': {
        templateUrl: "app/rules/rules.html",
      }
    }
  })
  .state('app.contact', {
    url: "/contact",
    views: {
      'mainContent': {
        templateUrl: "app/contact/contact.html",
      }
    }
  })
  .state('app.home', {
    url: "/home",
    views: {
      'mainContent': {
        templateUrl: "app/home/home.html",
      }
    }
  })
  .state('app.groups', {
    url: "/groups",
    views: {
      'mainContent': {
        templateUrl: "app/groups/groups.html",
      }
    }
  })
  .state('app.group-detail', {
    url: "/group/:id",
    views: {
      'mainContent': {
        templateUrl: "app/groups/group-detail.html"
      }
    }
  })
  .state('app.question', {
    url: "/question/:groupId/:id",
    views: {
      'mainContent': {
        templateUrl: "app/question/question.html"
      }
    }
  });

// if none of the above states are matched, use this as the fallback
$urlRouterProvider.otherwise('/app/home');
});
