var app = angular.module('eliteApp');
app.controller('QuestionCtrl', function($scope, $http, $stateParams) {
 
	groupId 	= Number ( $stateParams.groupId - 1);
	questionId 	= Number ( $stateParams.id - 1);
	console.log(questionId);
    $http.get("https://9finity.firebaseio.com/records/"+groupId+"/questions/"+questionId+".json")
    .then(function(response) {$scope.questions = response.data;});
    $http.get("https://9finity.firebaseio.com/records/"+groupId+".json")
    .then(function(response) {$scope.groupData = response.data;});
});